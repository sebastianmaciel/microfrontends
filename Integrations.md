# Sobre la Integración

**Aclaración**: Generalmente cuando se habla de <u>container</u>, <u>app contenedor</u> o <u>contenedor</u>, simplemente suele ser otro microfrontend que actúa como unificador y vinculador de las otra apps (microfrontends),

## Hay 3 categorías mayores de integración

**BUILD-TIME Integration** 

Se accede al código de nuestra app **antes** de que <u>el contenedor</u> se cargue en el browser.

**RUN-TIME Integration**

Se accede al código de nuestra app **después** de que <u>el contenedor</u> se cargue en el browser.

**SERVER Integration** 

Mientras se envía el JS para cargar <u>el contenedor</u>, el server decide si el código de nuestra app se incluye o no (más complicado y menos común).

___

## Flujo de ejemplo de *Build-Time Integration*

- El equipo desarrolla la app.
- Momento de deploy...
- Se publica la app como un **paquete NPM**.
- El equipo a cargo de la app **contenedor** instala nuestra app como una <u>dependencia</u>.
- Se hace build de la app contenedor.
- El <u>bundle de la app contenedor</u> incluye todo el código de nuestra app.

PROS: 

- Muy fácil de configurar y entender.

CONS: 

- El contenedor se debe **redeployar** cada vez que la app es actualizada.
- Puede haber tentativa de acoplar el contenedor y nuestra app, no recomendado.

---

## Flujo de ejemplo de *Run-Time Integration*

- El equipo desarrolla la app.
- Momento de deploy...
- Nuestra app se deploya en **https://work.site.com/NuestraApp.js**
- El usuario navega hasta **https://work.site.com** y se carga el contenedor.
- La app del contenedor busca nuestra app y la ejecuta.

PROS:

- Nuestra app se puede deployar independientemente del contenedor, en cualquier momento.
- Se pueden deployar <u>diferentes versiones</u> de nuestra app, y el contenedor puede decidir cuál mostrar al usuario.

CONS:

- Las herramientas y la configuración suelen ser **mucho más complicadas**.

---

