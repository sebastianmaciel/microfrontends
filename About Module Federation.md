## Sobre Module Federation

Es un plugin utilizado para exponer los archivos que queremos que estén disponible para las otras apps y por otro lado, desde dónde. 

- En el host o app contenedor, se configuran los **remotes**, que son las apps que van a poder accederse en el contenedor.
- En las apps hijas, se configuran los **exposes** para definir qué archivos y cómo van a estar disponibles.

### En nuestra app

Por un lado, Webpack se va a encargar de hacer nuestro proyecto **standalone**, que no es más que habilitar a que podamos correr nuestra app independientemente del contenedor, como un desarrollo normal a través de un típico `index.html`, el cual será reemplazado en el contenedor de otras maneras.

Por otro lado, al configurar nuestro **ModuleFederationPlugin**, Webpack va a estar preparando todo un set de archivos diferentes que contienen instrucciones de cómo cargar nuestra app de manera segura en el contenedor, cuando éste último lo requiera. Incluyendo nuestras carpetas, archivos y librerías que usamos en nuestro proyecto.

### En la app contenedor

Todo empieza en el **index.js**, que no es más que la oportunidad de darle a Webpack de que cargue todo lo necesario antes de que intente ejecutar nuestra app, o las apps hijas. Antes de ejectar el típico **bootstrap.js** y con eso correr las apps, Webpack se asegura de saber de dónde sacar todos los archivos necesarios y además, las dependencias de cada app.

### Boilerplate

**Para el contenedor:**

```javascript
new ModuleFederationPlugin({
    name: 'container',
    remotes: {
   		products: 'products@http://localhost:8081/remoteEntry.js'     
    },
}),
```

- `name`: En el contenedor no se usa para nada, solamente para aclaración a los desarrolladores.
- `remotes`: Es una lista de proyectos que el contenedor puede buscar para traerse código necesario.
- `remote.products`: Esta parte cargaría el archivo ubicado en la URL si en el contenedor alguien hace un import de `products`.
  - `products@...`: Hace referencia a la propiedad `name` en la misma configuración de webpack de nuestros proyectos.
  - `...@http://localhost:8081/remoteEntry.js`: la dirección de nuestro archivo.

**Para la app:**

```javascript
new ModuleFederationPlugin({
   name: 'products',
   filename: 'remoteEntry.js',
   exposes: {
       './ProductsIndex': './src/index', // Creando alias para los imports
   },
}),
```

- `name`: Debe ser exactamente igual al nombre con que se lo va a invocar en el contenedor, y viceversa.
- `exposes`: Setea el nombre del archivo de manifiesto. Normalmente nombrado `remoteEntry.js`
- `exposes[key]: [value]`:  El key es el alias que se puede usar para importar en el código, el value es el archivo a importar.

**Ejemplo:**

```javascript
// container|host app
import 'products/ProductsIndex';
```

**Aclaración:** La creación de alias para las importaciones es conveniente para mejorar la semántica y definición en nuestro código, no más que eso.
